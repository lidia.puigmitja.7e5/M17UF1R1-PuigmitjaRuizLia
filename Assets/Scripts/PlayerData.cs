
using UnityEngine;


public enum PlayerState
{
    Gigant,
    Normal,
    Crecendo,
    Decrecendo

}

public enum PlayerMovement
{
    StopRight,
    StopLeft,
    WalkRight,
    WalkLeft,
    UserMoviment

}


public enum PlayerType
{
    Patata,
    Mago,
    Hada,
    Dragon,
    Guard

}


public class PlayerData : MonoBehaviour
{

    public PlayerState Player_state;
    public PlayerMovement Player_movement;
    public PlayerType Player_type;
    public string PlayerName;
    public float Height;
    public float Weight;
    public float Distancia;
    public float Speed;
    public float tempsHabilitat;
    public float tempsRefredamentDefecte;
    private GameObject _player;
    [SerializeField]
    
   
    private float _waitTime = 0f;
    private Animator _animator;


    [SerializeField]
    private Sprite[] _sprite;

    // Nom,
    // Tipus de personatge,
    // al�ada, velocitat,
    // dist�ncia a rec�rrer. Aquest component ha de ser consultat per altres classes i editable des de l'inspector.

 
    private float _frame = 0;
    private float _framesTotal = 12;
    private float _tiempo;


    private Vector3 _minimScale = new Vector3(0.25f, 0.25f, 0.25f);
    private Vector3 _maximScale = new Vector3(1f, 1f, 1f);
    private Vector3 _changeScale = new Vector3(0.05f, 0.05f, 0.05f);
    private float tempsRefredament;
    [SerializeField]
    private float _totalSpeed;
    public float TotalSpeed
    {
        get => _totalSpeed;
        set => _totalSpeed = value;
    }
    //   private float _speed;
    private int fuegote;
    public void SetFuegote()
    {
        fuegote += 1;
    }
    public int GetFuegote()
    {
        return fuegote;
    }

    private void Awake()
    {
          _tiempo = Time.deltaTime;
        Distancia = 7f;
        _player = GameObject.Find("Player");
        Height = _player.transform.localScale.x;
        Weight = Height;
        Speed = 0.5f;
        _totalSpeed = Speed / Weight;
        Player_movement = PlayerMovement.WalkRight;
        tempsHabilitat = 10f;
        tempsRefredamentDefecte = 20f;
        tempsRefredament = tempsRefredamentDefecte;
        Player_type = PlayerType.Dragon;
        _animator = GetComponent<Animator>();
    }



    // Start is called before the first frame update
    void Start()
    {
        Player_state = PlayerState.Normal;


    }

    // Update is called once per frame
    void Update()
    {



        _tiempo += Time.deltaTime;
        Height = _player.transform.localScale.x;
        Weight = Height;
        _totalSpeed = Speed / Weight;

        if (_tiempo >= (1 / _framesTotal))
        {
            _frame++;
            _tiempo = 0;

            if (_frame == 3)
            {
               

                    _frame = 0;


                    ChangeState();

                




            }

        }
    }


    void ChangeState()
    {
       
        switch (Player_state)
        {
            case PlayerState.Normal:
                _player.transform.localScale = _minimScale;
               

                break;

            case PlayerState.Gigant:
                _player.transform.localScale = _maximScale;
                tempsHabilitat -= 1;
                if (tempsHabilitat == 0f)
                {
                    Player_state = PlayerState.Decrecendo;
                    tempsHabilitat = 10f;
                }
                break;

            case PlayerState.Crecendo:
                if (_player.transform.localScale != _maximScale) _player.transform.localScale += _changeScale;
                if (_player.transform.localScale == _maximScale) Player_state = PlayerState.Gigant;

                break;

            case PlayerState.Decrecendo:
                if (_player.transform.localScale != _minimScale) _player.transform.localScale -= _changeScale;
                if (_player.transform.localScale == _minimScale)
                {
                    tempsRefredament -= 1;
                    if (tempsRefredament <= 0)
                    {
                         Player_state = PlayerState.Normal;
                        tempsRefredament = tempsRefredamentDefecte;
                    }
                 

                }
                break;


        }

    }




}
