using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrigerFireball : MonoBehaviour
{
    private Transform _player;
    private Patrullar _patrullar;
    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player").GetComponent<Transform>();
        _patrullar = GameObject.Find("Player").GetComponent<Patrullar>();
    }

    // Update is called once per frame
    void Update()
    {
        MovimientoFantasma();
    }



    void MovimientoFantasma()
    {
        
        if (_patrullar.MovingRight == -1)
        {
            transform.position = new Vector3(_player.position.x - 0.75f, _player.position.y, _player.position.z);
        }
        if (_patrullar.MovingRight == 1)
        {
            transform.position = new Vector3(_player.position.x + 0.75f, _player.position.y, _player.position.z);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Fireball"))
        {
            _patrullar.MovingRight = -_patrullar.MovingRight;
        _patrullar.GestionarOrientacion();
       
        }
    }

}
