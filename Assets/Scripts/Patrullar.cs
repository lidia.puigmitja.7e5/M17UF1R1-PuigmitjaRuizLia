
using UnityEngine;


public class Patrullar : MonoBehaviour
{

    private ControlTime _timer;
    private float distance = 2f;
    private float _movingRight = 1;
    public float MovingRight
    {
        get => _movingRight;
        set => _movingRight = value;
    }

    private bool _muerto = false;
    public bool Muerto
    {
        get => _muerto;
        set => _muerto = value;
    }
    private Animator _animator;
    public float Speed;

    public Transform groundDetection;

    public float Height;
    public float Weight;
    public float _totalSpeed;
    private Vector2 screenBounds;
    private float playerHeight;
    private int caidalibre = 0;

    // Start is called before the first frame update
    void Start()
    {
        _timer=GameObject.Find("Timer").GetComponent<ControlTime>();
        groundDetection = GameObject.Find("GroundDetection").GetComponent<Transform>();
        _animator = GetComponent<Animator>();
        Height = transform.localScale.x;
        Weight = Height;
        Speed = 0.4f;
        _totalSpeed = Speed / Weight;
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        playerHeight = transform.GetComponent<SpriteRenderer>().bounds.size.y / 2;


    }

    // Update is called once per frame
    void Update()
    {
        if (Muerto == false)
        {
            ProcesarMovimientoAutonomo();
            MovimientoGroundDetection();
            TocaElSuelo();
            if (transform.position.y <= ((screenBounds.y * -1) - playerHeight))
            {
                PlayerDeath(caidalibre);
            }

        }

    }

    void MovimientoGroundDetection()
    {
        if (MovingRight == -1)
        {
            groundDetection.position = new Vector3(transform.position.x - 1.75f, transform.position.y, transform.position.z);
        }
        if (MovingRight == 1)
        {
            groundDetection.position = new Vector3(transform.position.x + 1.75f, transform.position.y, transform.position.z);
        }
    }

    void ProcesarMovimientoAutonomo()
    {
        transform.position += new Vector3(MovingRight * Time.deltaTime * _totalSpeed, 0, 0);
        _animator.SetFloat("Walk", _totalSpeed);
        

    }

    void TocaElSuelo()
    {
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, distance);

        if (groundInfo.collider == false)
        {
            MovingRight = -MovingRight;
            GestionarOrientacion();
        }


    }

    public void GestionarOrientacion()
    {

        if (MovingRight == 1)
        {
            Quaternion target = Quaternion.Euler(0, 0f, 0f);
            transform.rotation = target;
            _animator.SetFloat("Walk", 0);

            Debug.Log("Entra Right");
        }
        else
        {
            Quaternion target = Quaternion.Euler(0, 180f, 0f);
            transform.rotation = target;
            _animator.SetFloat("Walk", 0);

            Debug.Log("Entra Left");

        }
    }


    public void PlayerDeath(int numeroBola)
    {
        Muerto = true;
        _animator.SetBool("Death", Muerto);
        _animator.SetFloat("Walk", 0);
        Destroy(gameObject,_animator.GetCurrentAnimatorStateInfo(0).length);
        GameManager.Instance.PlayerAlive = false;
        GameManager.Instance.FireballDeath = numeroBola;
        _timer.CambioEscenaGameOver();
    }



}
