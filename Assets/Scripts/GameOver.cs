using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public class GameOver : MonoBehaviour
{
    private GameObject _playerDead;
    private GameObject _playerAlive;
    private GameObject _eye;
    private GameObject _eyegarra;
    private TMP_Text _textFinalGame;
    private TMP_Text _textFinalFireballs;
    // Start is called before the first frame update
    void Start()
    {
        _playerDead = GameObject.Find("PlayerDead");
        _playerAlive = GameObject.Find("PlayerAlive");
        _eye = GameObject.Find("eye");
        _eyegarra = GameObject.Find("eyegarra");
        _textFinalGame = GameObject.Find("TextFinal").GetComponent<TMP_Text>();
        _textFinalFireballs = GameObject.Find("FinalFireballs").GetComponent<TMP_Text>();
        ChargeScene();


    }

    // Update is called once per frame
    void Update()
    {
      
    }

    void ChargeScene()
    {
        switch (GameManager.Instance.gameType)
        {
            case GameManager.GameType.ModeSurvival:
       
        if (GameManager.Instance.TimeRunning == false && GameManager.Instance.PlayerAlive == true)
        {
            _eyegarra.SetActive(false);
            _playerDead.SetActive(false);
            _eye.SetActive(true);
            _playerAlive.SetActive(true);
            _textFinalGame.text = "Fuck yeah!\n" + GameManager.Instance.PlayerName + " you are the best of the universe!";
            _textFinalFireballs.text = "You have dodged " + GameManager.Instance.FireballsCount + " fireballs.";
        }
        else if (GameManager.Instance.TimeRunning == true && GameManager.Instance.PlayerAlive == false)
        {
            _eyegarra.SetActive(true);
            _playerDead.SetActive(true);
            _eye.SetActive(false);
            _playerAlive.SetActive(false);
            _textFinalGame.text = "Game Over!\nRest in peace in this fucking place " + GameManager.Instance.PlayerName + ".";
            
            if(GameManager.Instance.FireballDeath>0) _textFinalFireballs.text = "You were killed by the fireball " + GameManager.Instance.FireballDeath;
            else  _textFinalFireballs.text = "You were killed by gravity, don't try, you can't fly. :)";
            GameManager.Instance.PlayerAlive = true;
        }
                break;

            case GameManager.GameType.ModeDestruction:
                if (GameManager.Instance.TimeRunning == true && GameManager.Instance.PlayerAlive == false)
                {
                    _eyegarra.SetActive(false);
                    _playerDead.SetActive(true);
                    _eye.SetActive(true);
                    _playerAlive.SetActive(false);
                    _textFinalGame.text = "Fuck yeah!\n" + GameManager.Instance.PlayerName + " you are the best of the universe!";
                    if (GameManager.Instance.FireballDeath > 0) _textFinalFireballs.text = "You killed the intruder, you only needed " + GameManager.Instance.FireballDeath + " fireballs.";
                    else _textFinalFireballs.text = "You killed the intruder with the gravity, working smart, not hard. Good job! :)";
                    GameManager.Instance.PlayerAlive = true;
                }
                else if (GameManager.Instance.TimeRunning == false && GameManager.Instance.PlayerAlive == true)
                {
                    _eyegarra.SetActive(true);
                    _playerDead.SetActive(false);
                    _eye.SetActive(false);
                    _playerAlive.SetActive(true);
                    _textFinalGame.text = "Game Over!\nThe intruder has taken over the planet, how could you let him do that " + GameManager.Instance.PlayerName + "?";
                    _textFinalFireballs.text = "You spent the last " + GameManager.Instance.FireballsCount+ " fireballs we had.";
                   
                }
                break;

            default:
                break;


        }
    }
}
