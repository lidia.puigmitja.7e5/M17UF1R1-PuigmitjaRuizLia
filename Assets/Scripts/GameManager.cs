using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    [SerializeField]
    private bool _playerAlive;
    [SerializeField]
    private string _playerName;
    [SerializeField]
    private bool _timeRunning;
    [SerializeField]
    private int _fireballsCount=0;

    [SerializeField]
    private int _fireballDeath = 0;
    [SerializeField]
    private bool _changeScene=false;

    public bool ChangeScene
    {
        get => _changeScene;
        set=>_changeScene = value;
    }

    public enum GameType
    {
        ModeSurvival,
        ModeDestruction

    }

    public GameType gameType;
    //Game Instance Singleton
    public static GameManager Instance
    {
        get
        {
           
            return _instance;
        }
    }
    public bool PlayerAlive
    {
        get => _playerAlive;
        set => _playerAlive = value;

    }

    public int FireballsCount
    {
        get => _fireballsCount;
        set => _fireballsCount = value;

    }

    public int FireballDeath
    {
        get => _fireballDeath;
        set => _fireballDeath = value;

    }

    public string PlayerName
    {
        get => _playerName;
        set => _playerName = value;

    }

    public bool TimeRunning
    {
        get => _timeRunning;
        set => _timeRunning = value;
    }

    private void Awake()
    {
       if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
        DontDestroyOnLoad(this.gameObject);

        _playerAlive = true;
        _timeRunning = false;
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
      
    }

    
}
