
using UnityEngine;


public class MoveControl : MonoBehaviour
{


    private bool _muerto = false;
    public bool Muerto
    {
        get => _muerto;
        set => _muerto = value;
    }

    private Animator _animator;
    private float _tiempoMuerte = 0.9f;
    private float inputMovimiento;
    public LayerMask capaFloor;
    private float _speed;
    private Rigidbody2D _rigidbody;
    private CapsuleCollider2D _boxCollider;
    private bool _lookRight=true;
    private float fuerza;
    private float _distanceRay = 0.2f;


    private Vector2 screenBounds;
    private float playerHeight;
    private int caidalibre=0;
    private ControlTime _timer;

    private SpawnerControl spawner;

    // Start is called before the first frame update
    void Start()
    {
        _timer = GameObject.Find("Timer").GetComponent<ControlTime>();
        _speed = 5;
        _rigidbody = GetComponent<Rigidbody2D>();
        _boxCollider = GetComponent<CapsuleCollider2D>();
        fuerza = 5.50f;
        _animator = GetComponent<Animator>();

        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        playerHeight = transform.GetComponent<SpriteRenderer>().bounds.size.y / 2;

        spawner = GameObject.Find("Spawner").GetComponent<SpawnerControl>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Muerto == false)
        {
            ProcesarMovimiento();
            ProcesarSalto();
            GetAnimations();
            if(transform.position.y <= ((screenBounds.y * -1) - playerHeight)){
                PlayerDeath(caidalibre);
            }
           


        }
       
    }
    bool OnTheFloor()
    {
       RaycastHit2D raycastHit= Physics2D.BoxCast(_boxCollider.bounds.center, new Vector2(_boxCollider.bounds.size.x, _boxCollider.bounds.size.y), 0f, Vector2.down, _distanceRay, capaFloor);
       
       return raycastHit.collider != null;

    }

    void ProcesarSalto()
    {
        
        if ((Input.GetKeyDown(KeyCode.Space)) && OnTheFloor())
        {
            
            _rigidbody.AddForce(Vector2.up * fuerza, ForceMode2D.Impulse);
           

        }
            
       
         
        
    }

    void ProcesarMovimiento()
    {
        inputMovimiento = Input.GetAxis("Horizontal");
       

        _rigidbody.velocity = new Vector2(inputMovimiento * _speed, _rigidbody.velocity.y);

        GestionarOrientacion();
    }

    void GestionarOrientacion()
    {
        if((_lookRight==true && inputMovimiento<0) || (_lookRight==false && inputMovimiento > 0))
        {
            _lookRight = !_lookRight;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
        }

    }

    void GetAnimations()
    {
        _animator.SetFloat("Jump", _rigidbody.velocity.y);
        _animator.SetFloat("MoveHorizontal", inputMovimiento);
    }


     public void PlayerDeath(int numeroBola)
    {
        
        Muerto = true;
        _animator.SetBool("Death", Muerto);
        Destroy(gameObject,_animator.GetCurrentAnimatorStateInfo(0).length);
        GameManager.Instance.PlayerAlive = false;
        Debug.Log("Me mori");
        spawner.enabled = false;
        GameManager.Instance.FireballDeath=numeroBola;
        _timer.CambioEscenaGameOver();

    }


}
