using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class CambioSceneName : MonoBehaviour
{
    private Button _botton1;
    private Button _botton2;
    private string _name;

    private GameObject _popUp;
    // Start is called before the first frame update
    void Start()
    {

        _popUp = GameObject.Find("PopUp");
        _popUp.SetActive(false);
        _botton1 = GameObject.Find("StartTurnOn").GetComponent<Button>();
        _botton2 = GameObject.Find("StartSurvive").GetComponent<Button>();
        _botton1.onClick.AddListener(LoadStartTurnOn);
        _botton2.onClick.AddListener(LoadStartSurvive);
       



    }

    void LoadStartTurnOn()
    {
       
        if (GameObject.Find("Name").GetComponent<TMP_Text>().text.Length > 3)
        {
            _name= GameObject.Find("Name").GetComponent<TMP_Text>().text;
            GameManager.Instance.PlayerName= _name;
            GameManager.Instance.gameType = GameManager.GameType.ModeDestruction;
            SceneManager.LoadScene("GameAutonom");
        }
        else
        {
            //Muestra el pop-up
            _popUp.SetActive(true);
            //Funcion que se encarga de hacer esperar 1.5 segundos antes de desaparecer el pop.up. 
            StartCoroutine(CloseAfterTime(1.5f));
        }
        }

    void LoadStartSurvive()
    {
       
        if (GameObject.Find("Name").GetComponent<TMP_Text>().text.Length > 3)
        {
            _name = GameObject.Find("Name").GetComponent<TMP_Text>().text;
            GameManager.Instance.PlayerName = _name;
            GameManager.Instance.gameType = GameManager.GameType.ModeSurvival;
            SceneManager.LoadScene("GameSurvival");
        }
        else
        {
            //Muestra el pop-up
            _popUp.SetActive(true);
            //Funcion que se encarga de hacer esperar 1.5 segundos antes de desaparecer el pop.up. 
            StartCoroutine(CloseAfterTime(1.5f));
        }
    }

    public IEnumerator CloseAfterTime(float t)
        {
            yield return new WaitForSeconds(t);
            _popUp.SetActive(false);
        }
    
     
     
    

    // Update is called once per frame
    void Update()
    {
        
    }
}
