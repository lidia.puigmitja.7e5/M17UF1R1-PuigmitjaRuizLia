using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlTime : MonoBehaviour
{
    [SerializeField]
    private float _time = 30f;
    private string _textTime;


    // Start is called before the first frame update
    void Start()
    {
        _textTime = GetComponent<TMP_Text>().text;
        ControlTimer();
    }


    // Update is called once per frame
    void Update()
    {
        ControlTimer();
    }

    private void ControlTimer()
    {
        if (_time > 0)
        {
            GameManager.Instance.TimeRunning = true;

            _time -= Time.deltaTime;
            _textTime = Mathf.Round(_time).ToString();
            GetComponent<TMP_Text>().text = _textTime;

        }
        if(_time<=0)
        {
            GameManager.Instance.TimeRunning = false;
            CambioEscenaGameOver();

        }

    }
    public IEnumerator CloseAfterTime(float t)
    {
        GameManager.Instance.ChangeScene = true;
        yield return new WaitForSeconds(t);
        SceneManager.LoadScene("FinalScene");


    }

    public void CambioEscenaGameOver()
    {
        if (GameManager.Instance.PlayerAlive == false && GameManager.Instance.TimeRunning == true && GameManager.Instance.ChangeScene == false)
        {

            StartCoroutine(CloseAfterTime(1f));

        }

        if (GameManager.Instance.PlayerAlive == true && GameManager.Instance.TimeRunning == false && GameManager.Instance.ChangeScene == false)
        {
            StartCoroutine(CloseAfterTime(2f));

        }
    }


}
