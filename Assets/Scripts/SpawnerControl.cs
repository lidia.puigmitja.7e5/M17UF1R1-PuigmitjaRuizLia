using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerControl : MonoBehaviour
{
  
    public GameObject SmallFirePrefab;

    float tiempo = 0;
    float intervalo = 0;
    float framesTotal = 12;

    private PolygonCollider2D _colliderFloor;

    // Start is called before the first frame update
    void Start()
    {
        
        _colliderFloor = GameObject.Find("Floor").GetComponent<PolygonCollider2D>();
      


    }

    // Update is called once per frame
    void Update()
    {
       
         tiempo += Time.deltaTime;
        if (GameManager.Instance.PlayerAlive == true && GameManager.Instance.TimeRunning == false)
        {
            gameObject.GetComponent<SpawnerControl>().enabled = false;

        }
        if (tiempo >= 1 / framesTotal)
        {
            intervalo++;
            tiempo = 0;

            if (intervalo == 4)
            {
                Vector3 newPos = new Vector3(Random.Range(-(_colliderFloor.bounds.size.x / 2), _colliderFloor.bounds.size.x / 2), transform.position.y, transform.position.z);
                Instantiate(SmallFirePrefab, newPos, Quaternion.identity);
                GameManager.Instance.FireballsCount++;
                intervalo = 0;
            }
        }

        
      

      

        
    }
}
