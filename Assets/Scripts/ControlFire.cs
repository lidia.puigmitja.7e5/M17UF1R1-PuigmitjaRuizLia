using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlFire : MonoBehaviour
{
    private float radio = 2f;
    private float fuerzaExplosion = 250f;


    private MoveControl _player;
    private Patrullar _patrullar;
    private Animator _animator;
    public bool _choke;
    [SerializeField]
    private int _numeroBola;

    // Start is called before the first frame update
    void Start()
    {

        _choke = false;
        _animator = GetComponent<Animator>();
        _animator.SetBool("Choke", _choke);
        _player = GameObject.Find("Player").GetComponent<MoveControl>();
        _patrullar = GameObject.Find("Player").GetComponent<Patrullar>();
        _numeroBola = GameManager.Instance.FireballsCount;

        if (GameManager.Instance.gameType == GameManager.GameType.ModeSurvival)
        {
            fuerzaExplosion = 250f;
        }
        else
        {
            fuerzaExplosion = 100f;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

      

        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("me choqu� con el prota!");
            _choke = true;
            _animator.SetBool("Choke", _choke);

            Destroy(gameObject, _animator.GetCurrentAnimatorStateInfo(0).length);
            _choke = false;

            if (GameManager.Instance.gameType == GameManager.GameType.ModeSurvival)
            {
                _player.PlayerDeath(_numeroBola);
            }
            if (GameManager.Instance.gameType == GameManager.GameType.ModeDestruction)
            {
                _patrullar.PlayerDeath(_numeroBola);
            }


        }

        if (collision.gameObject.tag == "Floor")
        {
            Debug.Log("me choqu�!");
            _choke = true;
            _animator.SetBool("Choke", _choke);
            Explosion();
            Collider2D collider = gameObject.GetComponent<Collider2D>();
            Rigidbody2D rigthbody = gameObject.GetComponent<Rigidbody2D>();
            rigthbody.constraints = RigidbodyConstraints2D.FreezePosition;

            collider.isTrigger = true;
            Destroy(gameObject, _animator.GetCurrentAnimatorStateInfo(0).length);
            _choke = false;

        }
    }



    // Update is called once per frame
    void Update()
    {
    }

    public void Explosion()
    {
        //circulo alrededor d la bomba, guardar objetos q toque en arreglo
        Collider2D[] objetos = Physics2D.OverlapCircleAll(transform.position, radio);
        foreach (Collider2D colisionador in objetos)
        {
            Rigidbody2D rb2d = colisionador.GetComponent<Rigidbody2D>();
            if (colisionador.tag == "Player")
            {
                if (rb2d != null)
                {
                    Vector2 direccion = colisionador.transform.position - transform.position;
                    float distancia = direccion.magnitude;
                    float fuerzafinal = fuerzaExplosion / distancia;
                    rb2d.AddForce(direccion * fuerzafinal);
                    Debug.Log(colisionador.transform.position);
                }
            }
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radio);
    }

}
