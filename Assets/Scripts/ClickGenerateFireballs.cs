using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickGenerateFireballs : MonoBehaviour
{
    private Camera _camara;
    private float timeSpawnear = 0.5f;
    public GameObject fireballsPrefab;
    private Vector2 screenBounds;
    private float fireballHeight;
  //  private int clicksCounter;
    private float IntervaloSpawneo;

    // Start is called before the first frame update
    void Start()
    {
        _camara=Camera.main;
        fireballHeight = fireballsPrefab.transform.GetComponent<SpriteRenderer>().bounds.size.y / 2;
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));

    }

    // Update is called once per frame
    void Update()
    {
        IntervaloSpawneo-=Time.deltaTime;

        if (Input.GetMouseButtonDown(0))
        {
            if (BoomFireClick())
            {
                Spawnear(_camara.ScreenToWorldPoint(Input.mousePosition));
                GameManager.Instance.FireballsCount++;
            }
        }
    }


    private bool BoomFireClick()
    {
        if (IntervaloSpawneo > 0)
        {
            IntervaloSpawneo -= Time.deltaTime;
            return false;
        }
        else
        {
            IntervaloSpawneo = timeSpawnear;
            return true;
        }

    }

    private void Spawnear(Vector3 mousePosition)
    {
        mousePosition.z = 0;
        mousePosition.y = screenBounds.y+fireballHeight;
        Instantiate(fireballsPrefab, mousePosition, Quaternion.identity);
        Debug.Log("Screen: "+screenBounds.y);
        Debug.Log("Fire: " + fireballHeight);

    }

}
