using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayAgainOrMenu : MonoBehaviour
{

    private Button _botton1;
    private Button _botton2;
    // Start is called before the first frame update
    void Start()
    {
        _botton1 = GameObject.Find("PlayAgain").GetComponent<Button>();
        _botton2 = GameObject.Find("GoMenu").GetComponent<Button>();
        _botton1.onClick.AddListener(PlayAgainReset);
        _botton2.onClick.AddListener(GoToMenu);

    }

    // Update is called once per frame
    void Update()
    {
       
    }

    void PlayAgainReset()
    {
        switch (GameManager.Instance.gameType)
        {
            case GameManager.GameType.ModeSurvival:
                Resetear();
                SceneManager.LoadScene("GameSurvival");

                break;

            case GameManager.GameType.ModeDestruction:
                Resetear();
                SceneManager.LoadScene("GameAutonom");
                break;

            default:
                break;
        }
    }
  

    void Resetear()
    {
        GameManager.Instance.FireballDeath = 0;
        GameManager.Instance.FireballsCount = 0;
        GameManager.Instance.PlayerAlive = true;
        GameManager.Instance.ChangeScene=false;
    }

    void GoToMenu()
    {
        Resetear();
        GameManager.Instance.PlayerName = null;
        SceneManager.LoadScene("Menu");
    }


  }